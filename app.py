from flask import Flask, render_template

app = Flask(__name__)

class_roster = [('Bob', 78, 'Freshman'), ('Bober', 86, 'Sophomore'), ('Bobert', 88, 'Sophomore'),
                ('Bobera', 95, 'Junior'), ('Bobertto', 55, 'Junior'), ('Bobb', 99, 'Senior'),
                ('Bo', 100, 'Senior')]

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/welcome/<string:name>')
def welcome(name):
    return render_template('welcome.html', name=name)

@app.route('/roster/<int:grade_view>')
def roster(grade_view):
    return render_template('roster.html', grade_view=grade_view, roster=class_roster)


if __name__ == '__main__':
    app.run()
